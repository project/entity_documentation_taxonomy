README.txt
==========

A submodule of Entity Documentation module that exports configuration of taxonomies.

AUTHOR/MAINTAINER
======================
Author: Thanos Nokas(Matrixlord)
Maintainer: Thanos Nokas(Matrixlord) (https://drupal.org/user/1538394)
